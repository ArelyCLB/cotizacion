const express = require("express");
const router = express.Router();
const bodyparser = require("body-parser");

let datos =[{
    matricula: "2020030657",
    nombre: "LOAIZA BOJORQUEZ ARELY CONSUELO",
    sexo: "F", 
    materias: ["Ingles", "Base de datos", "Tecnologia I"]
},{
    matricula: "2020030311",
    nombre: "VALDIVIEZO GOMEZ MICHELLE ALEJANDRA",
    sexo: "F", 
    materias: ["Ingles", "Base de datos", "Tecnologia I"]
},{
    matricula: "2019030399",
    nombre: "ACOSTA ORTEGA JESUS HUMBERTO",
    sexo: "M", 
    materias: ["Ingles", "Base de datos", "Tecnologia I"]
},{
    matricula: "202003007",
    nombre: "ALMOGABAR VAZQUEZ YARLEN DE JESUS",
    sexo: "F", 
    materias: ["Ingles", "Base de datos", "Tecnologia I"]
}
]

router.get("/", (req,res)=>{
    /res.send("Iniciando servidor 3000 para un nuevo servidor web");/
    res.render('index.html', {titulo: "Mi Pagina", 
                nombre: "Arely Consuelo Loaiza Bojorquez", 
                grupo: "TI 8-3", 
                listado:datos});

})

router.get("/tabla", (req,res)=>{
    const params ={
        numero : req.query.numero
    }
    res.render('tabla.html', params);
})

router.post("/tabla", (req,res)=>{
    const params ={
        numero : req.body.numero
    }
    res.render('tabla.html', params);
})

router.get("/cotizacion", (req,res)=>{
    const params ={
        valor : req.query.valor,
        pInicial : req.query.pInicial,
        plazo: req.query.plazo
    }
    res.render('cotizacion.html', params);
})

module.exports = router;